# Database Experiment

The goal of this repository is to establish an easy way to compare different database types 
regarding performance and scalability. 
Therefore, a [USE_CASE](#use-case) is generically analyzed with the [BENCHMARK](#benchmark). 
To start up stacks easily, [DOCKER_COMPOSE_FILES](#docker-compose-files) are available. 
You may [EXTEND](#extend) the stacks as desired.
Make sure that your computer has enough [RESOURCES](#resources).

## Use Case

The use case is to store messages between a user and a chatbot and make them available. 
For this, a database must provide three primary methods:

1. storing individual messages with relevant metadata (referred to as write).
2. retrieving all chats of a user (referred to as list).
3. retrieving all messages of a chat from a user (referred to as read).

Other methods, such as deleting messages, are conceivable, 
but it is assumed that these are executed less frequently 
and have correspondingly less impact on performance.

A message generally has a
USER_ID, CHAT_ID, MESSAGE_ID, SENDER(User/Bot), TIMESTAMP and the CONTENT. 
This data should be stored as unintrusively as possible. 
The use of JOIN is avoided to implement only constant or linear complexity 
and to make the results comparable between SQL and NoSQL.

## Docker Compose Files

Docker compose files should be kept as platform-independent as possible, 
but it is possible that minor customizations will be necessary. 
Currently, four stacks are provided. 

1. PostgreSQL as a representative for SQL.
2. OpenSearch as a representative for document-based NoSQL databases.
3. MySQL operated as a cluster as a representative for NewSQL.
4. CockRoachDB to validate the NewSQL results.

Start only one stack at a time, 
make sure all previously launched stacks are shut down 
and wait for the current stack to fully start.

You may want to change the number of nodes per stack by changing the `create_compose_file.py`.
Note, however, that a vertical scaling database should only have one instance.
Use the corresponding `start_stack.sh` files to start the stack.

## Benchmark

### Concept

The write, list and read methods are processed one after the other. 
Each message resulting from the settings 
users, chats_per_user, messages_per_chat and message_length
is written/retrieved. 
Generally, the latency and the throughput are measured. 

Latency is defined as the response time of the server when the database is not busy. 
This is measured by sending only one request per time unit.

Throughput is the maximum amount of data that can be processed in a given time unit. 
This implies maximum server utilization.
It is simulated by creating a [multiprocessing.threadpool](https://docs.python.org/3/library/multiprocessing.html) 
with [THREAD_COUNT](app/utils/config.py) cores and sending all database requests as fast as possible.

Realistically, the speed of the database is determined by both latency and throughput.
How much one should prioritize one over the other depends on the use-case of the database.

The results are provided as a csv file. The exact path can be defined in the [CONFIG](app/utils/config.py).

### Getting Started

1. Install [POETRY](https://python-poetry.org/docs/).
2. Install the dependencies:

    ```
    poetry install
    ```

3. Change the settings by creating a `config.env` file at the top level of this repository:

   ```
   vi config.env
   ```
    
    Example File:   

    ```
    users_throughput=1000
    users_latency=100
    chats_per_user=5
    messages_per_chat=10
    message_length=10000
    threads=20
    batches=2000

    sql=True
    connection_string="sqlite://"  # in memory SQLite
    ```

4. Make sure a [DOCKER_COMPOSE_STACK](#docker-compose-files) is running.
5. Start jupyter notebook:

   ```
   poetry run python jupyter notebook
   ```
   
6. Navigate to the notebook you want to test and run all cells, or create a new notebook with the same schema.
7. Wait until the benchmark has finished.

## Extend

You may extend this demonstration with other database types. 
A docker-compose-file must be set up in an appropriate folder under [<PROJECT-FOLDER>/database_stacks](database_stacks). 
Don't forget to extend this documentation.

If the database type supports SQL or OpenSearch queries by default, 
the existing code can be reused. 
Otherwise, another connector must be created. 
Implement [CONNECTION_BASE](app/database_connections/opensearch_connection.py) for this purpose.

Finally, the [CONFIG](app/utils/config.py) must be adapted.

## Resources

This project determines horizontal scalability by running multiple instances each with one core and 1GB RAM.
For Databases that only scale vertically, a single big instance with equivalent computing power is created.
In addition, multiple cores are required for the benchmark. 
Accordingly, the performance requirements for the computer used to run the benchmark are high. 
A stack with 10 nodes and 8 cores to run the benchmark already requires:

1. Ten cores and 10GB RAM on the computer running the stack.
2. Another eight cores and about 2GB RAM on the PC running the benchmark.
3. At least one core and 2GB RAM extra to make sure the System does not run into hardware limits.

If the benchmark and the stack are running on the same Computer, the requirements are additive.
For the given example, 20 cores and 14GB RAM should be the minimum of computing power!

## Experiment

### Conduction

For each of the four databases, the docker-compose-stack was started.
It was waited until the stack was fully up and running.

The jupyter notebooks were used to run the tests. 
Therefore, the following settings were used:

````json
{
   "users_throughput": 1000,
   "users_latency": 100,
   "chats_per_user": 5,
   "messages_per_chat": 10,
   "message_length": 10000,
   "threads": 20,
   "batches": 2000
}
````

Each NoSQL or NewSQL cluster ran with 10 nodes, while PostgreSQL ran with only 1 node.
Each node regardless of the database and the node-amount of the cluster was limited to exactly one CPU and 2GB of RAM.

### Results

The following table shows the Operations per second for each database benchmark combination.

| Benchmark        | PostgreSQL | OpenSearch | MySQL | CockRoachDB |
|------------------|------------|------------|-------|-------------| 
| write throughput | 3092       | 49         | 1155  | 318         |
| list throughput  | 436        | 890        | 159   | 6           |
| read throughput  | 298        | 515        | 1     | 4           |
| write latency    | 312        | 2          | 1016  | 168         |
| list latency     | 258        | 395        | 73    | 3           |
| read latency     | 209        | 380        | 0     | 1           |

OpenSearch is alot faster in searching data than any other database.
This is especially true for throughput search tasks, but even latency search was faster than a single node PostgreSQL.
Storing data in OpenSearch does take significantly more time than for any other database, though.
This makes sense, as the database is heavily optimized for searching.
Thus, the searching speed is high, but when storing data alot of indexing must be done (see no free lunch theorem).
It should be noted, however, that the database was not used as intended. 
To make the results comparable and the benchmark runs through successfully, a "refresh=wait_for" was set.
With eventual consistency, the database would be able to store data much faster.

Comparing MySQL to PostgreSQL, it appears that MySQL is faster in writing, yet slower in searching than PostgreSQL.
This is surprising, as PostgreSQL runs only a single node (CPU: 1, RAM: 2GB), 
while MySQL has 10 nodes (each CPU: 1, RAM: 2GB).
Expecting linear scalability, MySQL should have been around 10 times faster than PostgreSQL.
When running the container, only one server-node has a high-CPU usage, 
so it might be that the cluster is configured incorrectly.
That's why a 2nd NewSQL database was added: CockRoach

When using CockRoach, the nodes had an even CPU usage.
Despite this fact, CockRoachDB was even slower than MySQL. 
Another issue using this database was: 
https://www.cockroachlabs.com/docs/v23.1/transaction-retry-error-reference#abort_reason_aborted_record_found.
Apparently, a "Cluster overload"
will cause the database to fail transactions far more often than any other database tested.

### Interpretation

OpenSearch should be used when eventual consistency is enough or when write-speed does not matter.

Since two wrong configurations in a row seem unlikely, 
especially since CockRoachDB used all of its nodes evenly, 
the logical conclusion is that NewSQL databases would not scale linearly as expected.
Further testing needs to be conducted, whether this is due to "incorrect usage" of the databases,
incorrect configuration or an issue with linear scalability.

However, it is safe to say: NewSQL does not appear to be the better solution for any use-case.
Depending on the requirement of scalability, an SQL database seems to be easier to configure, 
less error prune and depending on the use-case maybe even faster.
