from typing import Dict, Any, List, Optional

NODE_COUNT: int = 10
CPU_COUNT: float = 1
MEMORY_AMOUNT: float = 2
EXPORT_FILE: str = "docker-compose.yml"

# ----------------------------------------- preparing information -----------------------------------------

PORT_1: int = 26257


def get_node(node_num: int) -> str:
    return """
    node%NODE_NUM%:
        image: cockroachdb/cockroach:latest
        networks:
            - cluster
        command: start --insecure --advertise-addr=node%NODE_NUM%:%PORT_100% --listen-addr=node%NODE_NUM%:%PORT_100% --sql-addr=node%NODE_NUM%:%PORT% --join=%NODE_MAPPING%
        deploy:
            resources:
                limits:
                    cpus: '%CPU_COUNT%'
                    memory: %MEMORY_AMOUNT%G
        ports:
            - %PORT%:%PORT%""".replace(
            "%PORT%", str(PORT_1 + node_num)
        ).replace("%NODE_NUM%", str(node_num))


content: str = """version: "1"
services:
%services%

networks:
    cluster:
        driver: bridge
""".replace(
    "%services%", "\n".join([get_node(node_num=num) for num in range(NODE_COUNT)])
).replace(
    "%NODE_MAPPING%",
    ",".join([f"node{num}:%PORT_100%" for num in range(NODE_COUNT)]),
).replace(
    "%PORT_100%", str(PORT_1 + 100)
).replace(
    "%CPU_COUNT%", str(CPU_COUNT)
).replace(
    "%MEMORY_AMOUNT%", str(MEMORY_AMOUNT)
)

with open(EXPORT_FILE, "w") as file:
    file.write(content)
