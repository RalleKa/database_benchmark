from typing import Dict, Any, List, Optional

NODE_COUNT: int = 10
CPU_COUNT: int = 1
MEMORY_AMOUNT: int = 2
EXPORT_FILE: str = "docker-compose.yml"

# ----------------------------------------- preparing information -----------------------------------------

network_name: str = "cluster"

extra: str = """
         environment:
                MYSQL_ROOT_PASSWORD: password
                MYSQL_DATABASE: Message
                MYSQL_USER: user
                MYSQL_PASSWORD: password
                MYSQL_ROOT_HOST: '%'
        ports:
          - 3306:3306
""".lstrip()


management_info: Dict[str, Any] = {
    "id": "ndb_mgmd",
    "name": "management",
    "amount": 1,
    "extra": None,
}
data_info: Dict[str, Any] = {
    "id": "ndbd",
    "name": "data",
    "amount": 7,
    "extra": None,
}
server_info: Dict[str, Any] = {
    "id": "mysqld",
    "name": "server",
    "amount": 2,
    "extra": extra,
}

nodes: List[Dict[str, Any]] = [
    {"type_num": type_num, **info}
    for info in [management_info, data_info, server_info]
    for type_num in range(info.get("amount"))
]

# ----------------------------------------- docker compose file -----------------------------------------


def get_node(
    type_id: str, type_name: str, type_num: int, node_num: int, extra: Optional[str]
) -> str:
    return (
        """
    %TYPE_NAME%%TYPE_NUM%:
        image: mysql/mysql-cluster:latest
        volumes:
         - ./mysql-cluster.cnf:/etc/mysql-cluster.cnf
        networks: 
            %NETWORK_NAME%:
                ipv4_address: 192.168.0.%NODE_NUM+1%
        command: %TYPE_ID% --ndb-nodeid=%NODE_NUM%
        deploy:
            resources:
                limits:
                    cpus: '%CPU_COUNT%'
                    memory: %MEMORY_AMOUNT%G
        %EXTRA%""".replace(
            "%TYPE_NAME%", type_name
        )
        .replace("%TYPE_NUM%", str(type_num + 1))
        .replace("%NODE_NUM%", str(node_num + 1))
        .replace("%NODE_NUM+1%", str(node_num + 2))
        .replace("%TYPE_ID%", type_id)
        .replace("%NETWORK_NAME%", network_name)
        .replace("%CPU_COUNT%", str(CPU_COUNT))
        .replace("%MEMORY_AMOUNT%", str(MEMORY_AMOUNT))
        .replace("%EXTRA%", extra if extra and type_num == 0 else "")
    )


def get_nodes() -> List[str]:
    return [
        get_node(
            type_id=info.get("id"),
            type_name=info.get("name"),
            extra=info.get("extra"),
            type_num=info.get("type_num"),
            node_num=node_num,
        )
        for node_num, info in enumerate(nodes)
    ]


file_content: str = """version: "1"
services:
%nodes%

networks:
    %network_name%:
        ipam:
            driver: default
            config:
             - subnet: 192.168.0.0/16
"""
file_content = (
    file_content.replace("%nodes%", "".join(get_nodes()))
    .replace("%network_name%", network_name)
    .replace("%CPU_COUNT%", str(CPU_COUNT))
    .replace("%MEMORY_AMOUNT%", str(MEMORY_AMOUNT))
)

with open(EXPORT_FILE, "w") as file:
    file.write(file_content)

# ----------------------------------------- mysql-cluster.cnf -----------------------------------------


def get_description(type_id: str, node_num: int) -> str:
    return (
        f"[{type_id}]\n"
        f"NodeId={node_num + 1}\n"
        f"hostname=192.168.0.{node_num + 2}\n"
        + (f"datadir=/var/lib/mysql\n" if type_id != "mysqld" else "")
    )


def get_descriptions() -> List[str]:
    return [
        "[ndbd default]\n" "NoOfReplicas=1\n" "DataMemory=80M\n" "IndexMemory=18M\n"
    ] + [
        get_description(type_id=info.get("id"), node_num=node_num)
        for node_num, info in enumerate(nodes)
    ]


with open("mysql-cluster.cnf", "w") as file:
    file.write("\n".join(get_descriptions()))
