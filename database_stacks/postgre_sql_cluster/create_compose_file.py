CPU_COUNT: int = 1
MEMORY_AMOUNT: int = 2
EXPORT_FILE: str = "docker-compose.yml"


file_content: str = """version: "1"
services:

    postgres:
        image: postgres:latest
        environment:
          POSTGRES_USER: user
          POSTGRES_PASSWORD: password
          POSTGRES_DB: Message
        ports:
         - 5432:5432
        deploy:
          resources:
            limits:
              cpus: '%NODE_COUNT%'
              memory: %MEMORY_AMOUNT%G
""".replace(
    "%NODE_COUNT%", str(CPU_COUNT)
).replace(
    "%MEMORY_AMOUNT%", str(MEMORY_AMOUNT)
)


with open(EXPORT_FILE, "w") as file:
    file.write(file_content)
