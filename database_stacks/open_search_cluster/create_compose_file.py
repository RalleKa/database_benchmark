from typing import List

NODE_COUNT: int = 10
CPU_COUNT: int = 1
MEMORY_AMOUNT: int = 2
EXPORT_FILE: str = "docker-compose.yml"

nums: List[str] = [str(i) for i in range(NODE_COUNT)]

service_names: str = ",".join([f"opensearch-node{num}" for num in nums])

port: str = """
        ports:
          - 9200:9200 # REST API"""

services: List[str] = [
    """    opensearch-node%num%:
        image: opensearchproject/opensearch:latest
        container_name: opensearch-node%num%
        environment:
          - cluster.name=opensearch-cluster
          - node.name=opensearch-node%num%
          - discovery.seed_hosts=%service_names%
          - cluster.initial_cluster_manager_nodes=%service_names%
          - bootstrap.memory_lock=true # Disable JVM heap memory swapping
          - OPENSEARCH_JAVA_OPTS=-Xms%MEMORY_AMOUNT/2%M -Xmx%MEMORY_AMOUNT/2%M # Set min and max JVM heap sizes to at least 50% of system RAM
        ulimits:
          memlock:
            soft: -1
            hard: -1
          nofile:
            soft: 262144
            hard: 262144
        deploy:
          resources:
            limits:
              cpus: '%CPU_COUNT%'
              memory: %MEMORY_AMOUNT%G
        networks:
          - opensearch-net%port%""".replace(
        "%num%", num
    )
    .replace("%service_names%", service_names)
    .replace("%port%", port if num == "0" else "")
    .replace("%CPU_COUNT%", str(CPU_COUNT))
    .replace("%MEMORY_AMOUNT%", str(MEMORY_AMOUNT))
    .replace("%MEMORY_AMOUNT/2%", str(int(MEMORY_AMOUNT / 2 * 1024)))
    for num in nums
]

services_string: str = "\n\n".join(services)

volumes: str = "\n".join(
    ["""    opensearch-data%num%:""".replace("%num%", num) for num in nums]
)

frame: str = """
version: "1"
services:
%services%

volumes:
%volumes%

networks:
    opensearch-net:
""".replace(
    "%services%", services_string
).replace(
    "%volumes%", volumes
)

with open(EXPORT_FILE, "w") as file:
    file.write(frame)
