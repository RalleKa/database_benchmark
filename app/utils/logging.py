from datetime import datetime, timedelta
from logging import getLogger, Logger
from typing import TypeVar, Iterable, Any, Optional, Dict, List, Union, Type

from tqdm.asyncio import tqdm_asyncio
from tqdm.auto import tqdm as _tqdm

from app.utils.config import settings

E = TypeVar("E")


class OperationMetadata:
    def __init__(self, name: str, iterable: List[E], **kwargs) -> None:
        self.logger: Logger = getLogger("Operation")

        self.name: str = name
        self.iterable: List[E] = iterable
        self.kwargs: Dict[str, Any] = kwargs

        self.start_time: Optional[datetime] = None
        self.end_time: Optional[datetime] = None
        self.operations: Optional[int] = 0

    def __enter__(self) -> Union[tqdm_asyncio, Iterable[E]]:
        if self.start_time:
            raise PermissionError(
                f"Not allowed to start the same Operation twice.\n"
                f"Name: {self.name}\n"
                f"First Started: {self.start_time}\n"
                f"Current Time: {datetime.now()}"
            )

        # Only take the time after all checks were done.
        self.start_time = datetime.now()
        self.logger.info("Starting to %s at %s", self.name, self.start_time)
        return self.iterable

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        # Take the time as fast as possible. Only check for errors afterward.
        end_time: datetime = datetime.now()

        if not self.start_time:
            raise PermissionError(
                f"Can not end an Operation that has not started.\n"
                f"Name: {self.name}\n"
                f"Current Time: {end_time}"
            )

        if self.end_time:
            raise PermissionError(
                f"Not allowed to end the same Operation twice.\n"
                f"Name: {self.name}\n"
                f"First Started: {self.end_time}\n"
                f"Current Time: {end_time}"
            )

        self.end_time = end_time
        self.logger.info(
            "Finished to '%s' after %s. Operations per second: %s",
            self.name,
            self.elapsed_time,
            len(self.iterable)
            * self.kwargs.get("unit_scale", 1)
            / (self.elapsed_time.total_seconds() or 0.001),
        )

    def tqdm(
        self, iterable: Iterable[E], size: int
    ) -> tqdm_asyncio:
        return _tqdm(iterable=iterable, total=size, desc=self.name, **self.kwargs)

    @property
    def elapsed_time(self) -> timedelta:
        return self.end_time - self.start_time
