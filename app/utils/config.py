from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict()

    users_throughput: int
    users_latency: int
    chats_per_user: int
    messages_per_chat: int
    message_length: int

    threads: int
    batches: int

    sql: bool
    connection_string: str


settings: Settings = Settings(
    _env_file="./config.env", _env_file_encoding="utf-8", _case_sensitive=False
)
