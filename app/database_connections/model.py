from datetime import datetime
from typing import Any, Dict

from sqlalchemy import Enum, String, DateTime
from sqlalchemy.orm import DeclarativeBase, mapped_column, Mapped
import enum


class Participants(enum.Enum):
    user = "USER"
    bot = "BOT"


class Base(DeclarativeBase):
    pass


class Message(Base):
    __tablename__ = "Message"

    message_id: Mapped[str] = mapped_column(String(36), primary_key=True)
    user_id: Mapped[str] = mapped_column(String(36))
    chat_id: Mapped[str] = mapped_column(String(36))
    sender: Mapped[Participants] = mapped_column(Enum(Participants))
    utc_timestamp: Mapped[datetime] = mapped_column(DateTime())
    message: Mapped[str] = mapped_column(String(10000))

    def __init__(self, **entity) -> None:
        super().__init__(
            chat_id=entity.get("chat_id"),
            message_id=entity.get("message_id"),
            user_id=entity.get("user_id"),
            utc_timestamp=datetime.fromisoformat(entity.get("utc_timestamp"))
            if isinstance(entity.get("utc_timestamp"), str)
            else entity.get("utc_timestamp"),
            sender=Participants(entity.get("sender"))
            if isinstance(entity.get("sender"), str)
            else entity.get("sender"),
            message=entity.get("message"),
        )

    def dict(self) -> Dict[str, str]:
        body: Dict[str, str] = {}
        for key, value in self.__dict__.items():
            if isinstance(value, str):
                body[key] = value
            elif isinstance(value, datetime):
                body[key] = value.isoformat()
            elif isinstance(value, Participants):
                body[key] = value.value
        return body

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, Message):
            return False

        return (
            other.message_id == self.message_id
            and other.user_id == self.user_id
            and other.chat_id == self.chat_id
            and other.sender == self.sender
            and other.utc_timestamp == self.utc_timestamp
            and other.message == self.message
        )

    def __hash__(self) -> int:
        return hash(self.message_id)

    def __repr__(self) -> str:
        return (
            f"Message(message_id={self.message_id!r}, user_id={self.user_id!r}, chat_id={self.chat_id!r}, "
            f"utc_timestamp={self.utc_timestamp!r}, sender={self.sender!r}, message={self.message})"
        )
