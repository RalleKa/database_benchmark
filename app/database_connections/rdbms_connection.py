from typing import Optional, List

from sqlalchemy.orm import Session
from sqlalchemy import create_engine, or_, select, and_, Engine

from app.utils.config import settings
from app.database_connections.connection_base import ConnectionBase
from app.database_connections.model import Message, Base


class RDBMSConnection(ConnectionBase):
    def __init__(self, connection_string: Optional[str] = None):
        self.engine: Engine = create_engine(
            connection_string or settings.connection_string, echo=False, pool_size=settings.batches, pool_timeout=1000
        )
        Base.metadata.create_all(self.engine, tables=[Message.__table__])

    def add_message(self, messages: List[Message]) -> None:
        with Session(self.engine) as session:
            session.bulk_save_objects(messages)
            session.commit()

    def get_chats(self, user_ids: List[str]) -> List[str]:
        with Session(self.engine) as session:
            stmt = select(Message.chat_id).where(
                or_(*[Message.user_id == user_id for user_id in user_ids])
            )
            return [chat_id for chat_id in session.scalars(stmt)]

    def get_messages(self, chat_ids: List[str], user_ids: List[str]) -> List[Message]:
        with Session(self.engine) as session:
            stmt = (
                select(Message)
                .where(
                    or_(
                        *[
                            and_(Message.chat_id == chat_id, Message.user_id == user_id)
                            for chat_id, user_id in zip(chat_ids, user_ids)
                        ]
                    )
                )
                .order_by(Message.utc_timestamp)
            )
            return [user for user in session.scalars(stmt)]

    def delete_all(self) -> None:
        with Session(self.engine) as session:
            session.query(Message).delete()
            session.commit()
