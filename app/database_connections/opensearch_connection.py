from logging import getLogger, WARNING
from typing import List, Dict, Any, cast

from app.database_connections.model import Message
from app.database_connections.connection_base import ConnectionBase

from opensearchpy import OpenSearch

import urllib3

from app.utils.config import settings


class OpensearchConnection(ConnectionBase):
    MOCK_IP: str = "MOCK"

    def __init__(
        self, host_ip: str, port: int, user: str = "admin", password: str = "admin"
    ) -> None:
        # Deactivate Certification warnings only for Benchmarking purposes.
        # In productive Systems Certification must be added for security reasons.
        urllib3.disable_warnings()

        # set Logging to WARNING, so it does not affect performance.
        getLogger("opensearch").setLevel(WARNING)
        getLogger("urllib3.connectionpool").setLevel(WARNING)

        # create a client
        self.client = OpenSearch(
            hosts=[{"host": host_ip, "port": port}],
            http_auth=(user, password),
            use_ssl=True,
            verify_certs=False,
        )

        # create index
        self.index_name = "messages"
        # We use keywords, as we won't use full-text-search.
        # The performance can be optimized by using the strings "as-is",
        # instead of optimizing them for search.
        index_body = {
            "settings": {
                "index": {
                    "refresh_interval": "10s",
                }
            },
            "mappings": {
                "properties": {
                    "message_id": {"type": "keyword"},
                    "chat_id": {"type": "keyword"},
                    "user_id": {"type": "keyword"},
                    "utc_timestamp": {"type": "keyword"},
                    "sender": {"type": "keyword"},
                    "message": {"type": "keyword"},
                }
            },
        }
        if not self.client.indices.exists(self.index_name):
            self.client.indices.create(self.index_name, body=index_body)

    def add_message(self, messages: List[Message]) -> None:
        timeout: int = len(messages) * 20 + 10

        bulk_request: List[Dict[str, Any]] = []
        for message in messages:
            bulk_request.append(
                {"index": {"_index": self.index_name, "_id": message.message_id}}
            )
            bulk_request.append(message.dict())

        response: Dict[str, Any] = self.client.bulk(
            index=self.index_name,
            body=bulk_request,
            refresh="wait_for",
            request_timeout=timeout,
        )
        if response.get("errors", True):
            raise ConnectionError(f"Could not connect to OpenSearch: {response}")

    def get_chats(self, user_ids: List[str]) -> List[str]:
        timeout: int = len(user_ids) * 20 + 10

        search_body: Dict[str, Any] = {
            "size": 0,
            "aggs": {
                "chats": {
                    "filter": {"terms": {"user_id": user_ids}},
                    "aggs": {
                        "unique_chats": {
                            "terms": {
                                "field": "chat_id",
                                "size": len(user_ids) * settings.chats_per_user,
                            }
                        }
                    },
                }
            },
        }

        result: Dict[str, Any] = self.client.search(
            index=self.index_name, body=search_body, timeout=timeout
        )
        unique_chat_ids: List[str] = [
            bucket.get("key", "")
            for bucket in result.get("aggregations", {})
            .get("chats", {})
            .get("unique_chats", {})
            .get("buckets", [])
            if bucket.get("key", "")
        ]
        return unique_chat_ids

    def get_messages(self, chat_ids: List[str], user_ids: List[str]) -> List[Message]:
        timeout: int = len(user_ids) * 20 + 10

        search_body = {
            "size": len(chat_ids) * settings.messages_per_chat,
            "query": {
                "terms": {"chat_id": chat_ids},
            },
        }

        response: Dict[str, Any] = self.client.search(
            index=self.index_name, body=search_body, timeout=timeout
        )
        entities: List[Dict[str, str]] = [
            {key: value for key, value in hit.get("_source", {}).items()}
            for hit in response.get("hits", {}).get("hits", [])
        ]
        return [Message(**entity) for entity in entities]

    def delete_all(self) -> None:
        # It sometimes occurs that the database is not empty after running the bellow code.
        # As a quick and dirty fix, we just repeat the process until the database is empty.
        # This is, of course, not how thing should go; however,
        # since this is just a prototype and the speed of this operation is of no relevance, it is good enough for now.
        while True:
            # We to know the number of documents in the database to adjust the size parameter of the request.
            amount_before_deleting: int = (
                self.client.search(
                    index=self.index_name, body={"query": {"match_all": {}}}, size=0
                )
                .get("hits", {})
                .get("total", {})
                .get("value", -1)
            )
            timeout: int = amount_before_deleting * 20 + 10

            # when there are no documents, we don't need to delete anything
            if not amount_before_deleting:
                return

            # We will determine which documents to delete
            document_ids: List[str] = [
                cast(str, document.get("_id", ""))
                for document in self.client.search(
                    index=self.index_name,
                    body={"query": {"match_all": {}}},
                    size=amount_before_deleting,
                )
                .get("hits", {})
                .get("hits", {})
                if document.get("_id", "")
            ]

            # We then make a bulk delete, to save time.
            # It is essential to wait for the result, as this script may break with non-blocking operations.
            bulk_request: List[Dict[str, Any]] = [
                {"delete": {"_index": self.index_name, "_id": document_id}}
                for document_id in document_ids
            ]
            response: Dict[str, Any] = self.client.bulk(
                index=self.index_name,
                body=bulk_request,
                refresh="wait_for",
                request_timeout=timeout,
            )
            if response.get("errors", True):
                raise ConnectionError(f"Could not connect to OpenSearch: {response}")
