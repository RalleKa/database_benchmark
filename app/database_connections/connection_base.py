from typing import List

from app.database_connections.model import Message


class ConnectionBase:
    def add_message(self, messages: List[Message]) -> None:
        raise NotImplementedError()

    def get_chats(self, user_ids: List[str]) -> List[str]:
        raise NotImplementedError()

    def get_messages(self, chat_ids: List[str], user_ids: List[str]) -> List[Message]:
        # chat_ids and user_ids must have the same length
        raise NotImplementedError()

    def delete_all(self) -> None:
        raise NotImplementedError()
