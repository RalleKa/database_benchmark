import string
from datetime import datetime, timedelta
from logging import Logger, getLogger
from multiprocessing.pool import ThreadPool, Pool
from random import choice
from typing import List, Tuple, Optional
from uuid import uuid4

from app.database_connections.model import Message, Participants
from app.utils.config import settings
from app.utils.logging import OperationMetadata


class Dataset:
    letters: str = string.ascii_lowercase

    def __init__(
        self,
        users: Optional[int] = None,
        chats_per_user: Optional[int] = None,
        messages_per_chat: Optional[int] = None,
        message_length: Optional[int] = None,
        name: str = __name__
    ) -> None:
        self.logger: Logger = getLogger(name)
        self._current_time: datetime = datetime.now().replace(microsecond=0)

        self.users: int = users or settings.users
        self.chats_per_user: int = chats_per_user or settings.chats_per_user
        self.messages_per_chat: int = messages_per_chat or settings.messages_per_chat
        self.message_length: int = message_length or settings.message_length

        self.operations_per_second: float = -1
        self.messages: List[Message] = self._get_all_messages()

    @property
    def current_time(self) -> datetime:
        self._current_time += timedelta(seconds=1)
        return self._current_time

    def _get_all_messages(self) -> List[Message]:
        operation_metadata: OperationMetadata = OperationMetadata(
            name="prepare data for benchmark",
            iterable=list(range(self.users)),
            unit_scale=self.chats_per_user * self.messages_per_chat,
        )
        # Using a ThreadPool here will break the current_time parameter, which does not hava a locking mechanism.
        # However, since this is only to generate test data, it is considered to be ok.
        with operation_metadata as user_iterator:
            with Pool(processes=settings.threads) as pool:
                result: List[Message] = [
                    message
                    for messages_list in operation_metadata.tqdm(
                        iterable=pool.imap(
                            iterable=user_iterator,
                            func=self._get_user_messages,
                            chunksize=int(self.users / settings.batches) or 1
                        ),
                        size=self.users
                    )
                    for message in messages_list
                ]
        self.operations_per_second = (
                self.users * self.chats_per_user * self.messages_per_chat /
                operation_metadata.elapsed_time.total_seconds()
        )
        return result

    def _get_user_messages(self, *args) -> List[Message]:
        return [
            message
            for _ in range(self.chats_per_user)
            for message in self._get_chat_messages(
                user_id=str(uuid4()), chat_id=str(uuid4())
            )
        ]

    def _get_chat_messages(self, user_id: str, chat_id: str) -> List[Message]:
        sender: Participants = Participants.user
        for _ in range(self.messages_per_chat):
            message, sender = self._get_message(
                user_id=user_id,
                chat_id=chat_id,
                sender=sender,
            )
            yield message

    def _get_message(
        self,
        user_id: str,
        chat_id: str,
        sender: Participants,
    ) -> Tuple[Message, Participants]:
        message_content: str = "".join(
            choice(Dataset.letters) for _ in range(self.message_length)
        )
        message: Message = Message(
            user_id=user_id,
            chat_id=chat_id,
            message_id=str(uuid4()),
            sender=sender,
            utc_timestamp=self.current_time,
            message=message_content,
        )

        next_sender = (
            Participants.user if sender == Participants.bot else Participants.bot
        )

        return message, next_sender
