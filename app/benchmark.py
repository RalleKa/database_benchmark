import logging
import string
from datetime import datetime, timedelta
from logging import getLogger, Logger
from multiprocessing.pool import ThreadPool
from typing import Optional, List, Callable, Any, Dict, Set, Tuple

from pandas import DataFrame
from urllib3.connectionpool import xrange

from app.dataset import Dataset
from app.utils.config import settings, Settings
from app.database_connections.connection_base import ConnectionBase

from app.database_connections.model import Message
from app.utils.logging import OperationMetadata


class Benchmark:
    letters: str = string.ascii_lowercase

    def __init__(
        self,
        connection: ConnectionBase,
    ) -> None:
        self.connection: ConnectionBase = connection
        self.logger: Logger = getLogger(__name__)

        self.messages: Optional[List[Message]] = None

    @staticmethod
    def _run_test(
        function: Callable[[List[Any]], Any],
        batches: List[List[Any]],
        unit_scale: float,
        name: str,
    ) -> Tuple[timedelta, Set[Any]]:
        operation_metadata: OperationMetadata = OperationMetadata(
            name=name, iterable=batches, unit_scale=unit_scale
        )
        with ThreadPool(processes=settings.threads) as pool:
            with operation_metadata as iterable:
                result: List[List[Any]] = list(
                    operation_metadata.tqdm(
                        iterable=pool.imap(func=function, iterable=iterable),
                        size=len(iterable),
                    )
                )

        # flatten the results (if there are any)
        result_set: Set[Any] = {value for sublist in result for value in sublist or []}

        return operation_metadata.elapsed_time, result_set

    def _run_test_and_validation(
        self,
        function: Callable[[List[Any]], Any],
        iterable: List[Any],
        bulk: bool,
        name: str,
        expected: Set[Any],
    ) -> float:
        if bulk:
            batches: List[List[Message]] = [
                iterable[i :: settings.batches]
                for i in xrange(settings.batches)
                if iterable[i :: settings.batches]
            ]
            unit_scale: float = len(iterable) / len(batches)
        else:
            batches: List[List[Message]] = [[message] for message in iterable]
            unit_scale: float = 1

        elapsed, result = Benchmark._run_test(
            function=function,
            batches=batches,
            unit_scale=unit_scale,
            name=f"benchmark {name}",
        )
        self._validate_data(result=result, expected=expected, name=f"validate {name}")
        return len(iterable) / elapsed.total_seconds()

    @staticmethod
    def _validate_data(
        result: Set[Any],
        expected: Set[Any],
        name: str = "",
    ) -> None:
        operation_metadata: OperationMetadata = OperationMetadata(
            iterable=[1], name=name, unit_scale=1, total=1
        )

        with operation_metadata:
            if result != expected:
                first: int = 10
                raise AssertionError(
                    f"\nresult ({len(result)}), but not expected ({len(expected)}): ({first}/{len(result - expected)})\t{list(result - expected)[:10]}"
                    f"\nexpected ({len(expected)}), but not result ({len(result)}): ({first}/{len(expected - result)})\t{list(expected - result)[:10]}"
                )

    def prepare_dataset(self, bulk: bool, name: str = "prepare", _settings: Optional[Settings] = None) -> float:
        _settings = _settings or settings
        dataset: Dataset = Dataset(
            users=_settings.users_throughput if bulk else _settings.users_latency,
            chats_per_user=settings.chats_per_user,
            messages_per_chat=settings.messages_per_chat,
            message_length=settings.message_length,
            name=name
        )
        self.messages = dataset.messages
        return dataset.operations_per_second

    def run_write_test(self, bulk: bool, name: str = "write") -> float:
        if not self.messages:
            raise ValueError("messages are not yet initialized")

        return self._run_test_and_validation(
            function=self.connection.add_message,
            iterable=self.messages,
            bulk=bulk,
            name=name,
            expected=set(),
        )

    def run_list_test(self, bulk: bool, name: str = "list") -> float:
        if not self.messages:
            raise ValueError("messages are not yet initialized")

        return self._run_test_and_validation(
            function=self.connection.get_chats,
            iterable=list({message.user_id for message in self.messages}),
            bulk=bulk,
            name=name,
            expected={message.chat_id for message in self.messages},
        )

    def run_read_test(self, bulk: bool, name: str = "read") -> float:
        if not self.messages:
            raise ValueError("messages are not yet initialized")

        return self._run_test_and_validation(
            function=lambda parameters: self.connection.get_messages(
                user_ids=[parameter[0] for parameter in parameters],
                chat_ids=[parameter[1] for parameter in parameters],
            ),
            iterable=list(
                {(message.user_id, message.chat_id) for message in self.messages}
            ),
            bulk=bulk,
            name=name,
            expected={message for message in self.messages},
        )

    def clean_up(self) -> None:
        start_time: datetime = datetime.now()
        self.logger.info("Starting clean up at %s", start_time)

        self.connection.delete_all()

        end_time: datetime = datetime.now()
        elapsed: timedelta = end_time - start_time
        self.logger.info("Finished clean up after %s", elapsed)

    def run_all_tests(self) -> DataFrame:
        columns: Dict[str, bool] = {
            "throughput": True,
            "latency": False,
        }
        rows: Dict[str, Callable[[bool, str], float]] = {
            "prepare": self.prepare_dataset,
            "write": self.run_write_test,
            "list": self.run_list_test,
            "read": self.run_read_test,
        }

        result: Dict[str, Dict[str, int]] = {}
        self.clean_up()
        for column_name, bulk in columns.items():
            result[column_name] = {
                row_name: int(
                    function(bulk, f"{column_name}_{row_name}")
                )
                for row_name, function in rows.items()
            }
        self.clean_up()

        return DataFrame(result)
