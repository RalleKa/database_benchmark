from datetime import datetime
from typing import List, Dict, Union
from uuid import uuid4

from app.database_connections.connection_base import ConnectionBase
from app.database_connections.opensearch_connection import (
    OpensearchConnection,
)
from app.database_connections.rdbms_connection import RDBMSConnection
from app.database_connections.model import Message, Participants
from app.dataset import Dataset


def get_opensearch_connection() -> OpensearchConnection:
    # There is no working mock, that supports all operations we use. So instead, we use a real database.
    host_ip: str = "127.0.0.1"
    port: int = 9200
    return OpensearchConnection(host_ip=host_ip, port=port)


def get_sql_connection() -> RDBMSConnection:
    connection_string: str = "sqlite://"  # in memory
    return RDBMSConnection(connection_string=connection_string)


def test_opensearch_single() -> None:
    connection: OpensearchConnection = get_opensearch_connection()
    happy_path_single(connection=connection)


def test_rdbms_single() -> None:
    connection: RDBMSConnection = get_sql_connection()
    happy_path_single(connection=connection)


def test_opensearch_bulk() -> None:
    connection: OpensearchConnection = get_opensearch_connection()
    happy_path_bulk(connection=connection)


def test_rdbms_bulk() -> None:
    connection: RDBMSConnection = get_sql_connection()
    happy_path_bulk(connection=connection)


def get_random_message(sender: Participants) -> Dict[str, str]:
    message_dict: Dict[str, Union[str, datetime, Participants]] = {
        "message_id": str(uuid4()),
        "user_id": str(uuid4()),
        "chat_id": str(uuid4()),
        "utc_timestamp": datetime.now(),
        "sender": sender,
        "message": str(uuid4()),
    }
    return Message(**message_dict).dict()


def happy_path_single(connection: ConnectionBase) -> None:
    message: Dict[str, Union[str, datetime, Participants]] = {
        "message_id": "Some Message ID",
        "user_id": "Some User ID",
        "chat_id": "Some Chat ID",
        "utc_timestamp": datetime.now(),
        "sender": Participants.user,
        "message": "some message",
    }
    return happy_path(connection=connection, messages=[message])


def happy_path_bulk(connection: ConnectionBase) -> None:
    messages: List[Dict[str, Union[str, datetime, Participants]]] = [
        message.dict()
        for message in Dataset(
            users=2, chats_per_user=3, messages_per_chat=4, message_length=5
        ).messages
    ]
    return happy_path(connection=connection, messages=messages)


def happy_path(connection: ConnectionBase, messages: List[Dict[str, str]]) -> None:
    # at first, we delete all old data, that might exist, to ensure the test does not get falsified by old data.
    connection.delete_all()

    # Test add messages
    assert not connection.add_message(
        messages=[Message(**message) for message in messages]
    )

    # Test get chats
    assert set(
        connection.get_chats(
            user_ids=list(
                {
                    message.get("user_id")
                    for message in messages
                    if message.get("user_id")
                }
            )
        )
    ) == {message.get("chat_id") for message in messages if message.get("chat_id")}

    # Ensure that the get chats does not just return all chat_ids it stores
    assert set(connection.get_chats(user_ids=[messages[0].get("user_id")])) == {
        message.get("chat_id")
        for message in messages
        if message.get("user_id") == messages[0].get("user_id")
    }

    # Test get messages
    assert {
        message
        for message in connection.get_messages(
            chat_ids=[message.get("chat_id") for message in messages],
            user_ids=[message.get("user_id") for message in messages],
        )
    } == set(Message(**message) for message in messages)

    # Ensure that the get message method does not just return all messages it stores
    assert {
        message
        for message in connection.get_messages(
            chat_ids=[messages[0].get("chat_id")], user_ids=[messages[0].get("user_id")]
        )
    } == {
        Message(**message)
        for message in messages
        if message.get("chat_id") == messages[0].get("chat_id")
        and message.get("user_id") == messages[0].get("user_id")
    }

    # Test delete messages
    connection.delete_all()
    assert not connection.get_chats(
        user_ids=[message.get("user_id") for message in messages]
    )
