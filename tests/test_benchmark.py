from os import remove
from os.path import join, exists

from pandas import DataFrame

from app.benchmark import Benchmark
from app.database_connections.connection_base import ConnectionBase
from app.database_connections.opensearch_connection import OpensearchConnection
from app.database_connections.rdbms_connection import RDBMSConnection


class SQLiteFileMock:
    def __init__(self, filename: str) -> None:
        self.filename: str = filename

    def __enter__(self) -> "SQLiteFileMock":
        if exists(self.filename):
            remove(self.filename)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.__enter__()


def test_opensearch() -> None:
    # There is no working mock, that supports all operations we use. So instead, we use a real database.
    host_ip: str = "127.0.0.1"
    port: int = 9200
    connection: OpensearchConnection = OpensearchConnection(host_ip=host_ip, port=port)
    happy_path(connection=connection)


def test_rdbms() -> None:
    filename: str = join("tests", "test.db")

    with SQLiteFileMock(filename=filename):
        # can not do this in memory, as each process in the pool opened by "Benchmark" has its own memory.
        connection_string: str = f"sqlite:///{filename}"
        connection: RDBMSConnection = RDBMSConnection(
            connection_string=connection_string
        )
        happy_path(connection=connection)


def happy_path(connection: ConnectionBase) -> None:
    benchmark: Benchmark = Benchmark(
        connection=connection,
        users=2,
        chats_per_user=3,
        messages_per_chat=4,
        message_length=5,
    )
    benchmark_result: DataFrame = benchmark.run_all_tests()

    # DataFrame-structure test
    for line in ["latency", "throughput"]:
        for column in ["write", "list", "read"]:
            assert int(benchmark_result[line][column])

    # no need to test plausibility, as the data gets validated during benchmark
